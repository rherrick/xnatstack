#!/bin/bash

DEFAULT=$(grep ^default: default.yaml | sed "s/^.*:[[:space:]]*'\(.*\)'/\1/")
BOXES=($(grep "^    [A-z]" default.yaml | sed 's/^    //' | sed 's/:$//'))

[[ -z $1 ]] && {
    echo "Select one of the base boxes below ('*' indicates default):"
    echo ""
    INDEX=1
    for BOX in "${BOXES[@]}"; do
        [[ ${DEFAULT} == ${BOX} ]] && { PREFIX="* "; } || { PREFIX="  "; }
        echo "${PREFIX}${INDEX}) ${BOX}"
        INDEX=$(expr ${INDEX} + 1)
    done
    echo ""
    read -p "Select: " SELECT
    echo ""
    [[ -z ${SELECT} ]] && {
        BOX=${DEFAULT}
    } || {
        CHOICE=$(expr ${SELECT} - 1)
        BOX=${BOXES[${CHOICE}]}
    }
} || {
    BOX=$1
    if echo ${BOXES[@]} | grep -q -w "${BOX}"; then
        echo Box ${BOX} specified on the command line.
    else
        echo Box \"${BOX}\" specified on the command line is not a valid build option.
        echo Please select one of: ${BOXES[*]}
        exit -1
    fi
}

echo Building the XNAT Stack base box using the \"${BOX}\" configuration.
echo ""

STACKBOX="xnatstack-${BOX}"
[[ -f boxes/${STACKBOX}.box ]] && {
    echo The target box already exists at boxes/${STACKBOX}.box. Do you want to delete this
    echo file and re-run the build for this target?
    echo ""
    read -p "Select (y/N): " SELECT
    echo ""
    case "${SELECT}" in
        [yY][eE][sS]|[yY]) 
            rm -f boxes/${STACKBOX}.box
            ;;
        [nN][oO]|[nN]) 
            echo Quitting...
            exit -1
            ;;
        *)
            echo Unknown option, quitting...
            exit -2
            ;;
    esac
}

echo "build: '${BOX}'" > .build.yaml

vagrant up --no-provision

[[ $? != 0 ]] && { STATUS=$?; echo An error occurred during VM initialization: ${STATUS}. Please check the output messages.; exit $?; }

echo ""
echo Reloading the VM and running the provision script.
echo ""
vagrant reload --provision-with build

[[ $? != 0 ]] && { STATUS=$?; echo An error occurred during VM provisioning: ${STATUS}. Please check the output messages.; exit $?; }

echo ""
echo One last reload...
echo ""
vagrant reload

[[ $? != 0 ]] && { STATUS=$?; echo An error occurred during VM reload: ${STATUS}. Please check the output messages.; exit $?; }

[[ -d boxes ]] || { mkdir boxes; }

echo ""
echo "Packaging: ${STACKBOX}"
echo ""
vagrant package --output boxes/${STACKBOX}.box

echo ""
echo "Adding ${STACKBOX} to Vagrant box library."
echo ""
vagrant box add --force ${STACKBOX} boxes/${STACKBOX}.box

echo Build of the ${STACKBOX} box is complete. Would you like to destroy the VM for this box now?
echo You may want to retain it for forensics purposes if the box build failed, otherwise you can
echo safely delete this VM, since the important output is the box file.
echo ""
read -p "Select (Y/n): " SELECT
echo ""
case "${SELECT}" in
    [nN][oO]|[nN]) 
        echo Quitting without destroying VM.
        ;;
    *)
        echo Destroying VM...
        vagrant destroy --force
        ;;
esac

