#!/bin/bash

# XNAT is developed by the Neuroinformatics Research Group, http://nrg.wustl.edu
# http://www.xnat.org
# Copyright (c) 2015, Washington University School of Medicine
# All Rights Reserved
#
# Released under the Simplified BSD.
# 
# XNAT Stack Base Box
# 
# This Vagrant project creates a VM that can be saved as the XNAT Stack base box. This process started from the advice 
# from this article:
#
#  * https://scotch.io/tutorials/how-to-create-a-vagrant-base-box-from-an-existing-one#ssh-into-the-box-and-customize-it
#
# This particular implementation creates an Ubuntu 15.04 base VM with Docker pre-installed. It then adds a number of
# XNAT prerequisites and useful utilities, including:
#
#  * Ruby
#  * Mercurial
#  * Git
#  * Tomcat
#  * OpenJDK 8
#  * nginx
#  * PostgreSQL
#  * Tools including vim and tmux
#
# The full documentation for the XNAT Stack base box can be found in the accompanying README.md. License information
# can be found in LICENSE.txt.
#

echo Now building xnatstack on $(cat /etc/centos-release)

# Install the EPEL release packages with extras.
echo ""
echo ------------------------------------------------------------
echo Configuring EPEL and extras repositories
echo ------------------------------------------------------------
sudo yum -y install --enablerepo=extras epel-release

# Update all the packages.
sudo yum -y upgrade

# Install the repo info for Docker services, then Docker itself.
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce


echo ""
echo ------------------------------------------------------------
echo Installing OpenJDK 8
echo ------------------------------------------------------------
sudo yum -y install java-1.8.0-openjdk-devel

echo ""
echo ------------------------------------------------------------
echo Installing Nginx
echo ------------------------------------------------------------
sudo yum -y install nginx

echo ""
echo ------------------------------------------------------------
echo Installing Tomcat
echo ------------------------------------------------------------
sudo yum -y install tomcat tomcat-admin-webapps

echo ""
echo ------------------------------------------------------------
echo Installing Postgres 9.6
echo ------------------------------------------------------------
# Update to use PostgreSQL 9.6-3 PGDG
sudo yum install -y https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm
sudo yum install -y postgresql96-server
sudo /usr/pgsql-9.6/bin/postgresql96-setup initdb
sudo systemctl enable postgresql-9.6.service
sudo systemctl start postgresql-9.6.service

sudo yum -y install git mercurial zip unzip ruby vim-enhanced tmux
# dcmtk python-setuptools libpython2.7-dev python-pip libxml2-dev libxslt1-dev python-lxml libinsighttoolkit4-dev mricron dcm2niix

# Copy XNAT extension scripts and libraries into VM, make sure the scripts are executable.
sudo cp -R /vagrant/tools/* /usr/local
sudo chmod -R +x /usr/local/bin/*
[[ -d /usr/local/sbin ]] && { sudo find /usr/local/sbin -type f -exec chmod -R +x '{}' \;; }

sudo yum -y clean all
sudo rm -rf /var/cache/yum/*

# 
# Zero out the blank space then clear it.
echo An error message will appear: "dd: error writing ‘/EMPTY’: No space left on device"
echo This is expected.
sudo dd if=/dev/zero of=/EMPTY bs=1M
sudo rm -f /EMPTY

# Clear out the history.
cat /dev/null > ~/.bash_history && history -c && exit

